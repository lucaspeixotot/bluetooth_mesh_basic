/**
 * @file ic_board.h
 * @Lucas Peixoto (lpdac@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2018-12-30
 *
 * @copyright Copyright (c) 2018
 *
 */

#ifndef _IC_BOARD_H
#define _IC_BOARD_H

#include <misc/util.h>

#define MERGE_ARRAYS(_array_type, _a, _b, _dst) \
    u32_t az = ARRAY_SIZE(_a);                  \
    u32_t bz = ARRAY_SIZE(_b);                  \
    _array_type _dst[az + bz];                  \
    for (int i = 0; i < (az + bz); i++) {       \
        if (i < az) {                           \
            _dst[i] = _a[i];                    \
        } else {                                \
            _dst[i] = _b[i - az];               \
        }                                       \
    }

#endif