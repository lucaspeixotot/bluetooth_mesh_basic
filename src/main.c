/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2018-12-25
 *
 * @copyright Copyright (c) 2018
 *
 */

// #include "ic_board.h"
#include <pwm.h>

#include "ic_bt.h"
#include "ic_buttons.h"
#include "ic_version.h"

#define SLEEP_TIME 250

#define PWM_DRIVER CONFIG_PWM_NRF5_SW_0_DEV_NAME
#define PWM_CHANNEL0 LED0_GPIO_PIN
#define PWM_CHANNEL1 LED1_GPIO_PIN
#define PERIOD (USEC_PER_SEC / 50)
#define FADESTEP 2000
#define FADESTEP_LEVEL 6553

// Initializing variables
s16_t leds_levels[2]    = {S16_MIN, S16_MIN};
s16_t buttons_states[4] = {0};
void set_leds(u8_t pin, s16_t pulse_width);
struct k_work pressed_work[BUTTON_NUMBERS];
struct device *pwm_dev;
int ic_pwm_run_led(struct device *pwm_dev, u8_t pin, u16_t value);

static struct level_srv led_elems[] = {
    {
        .pin       = PWM_CHANNEL0,
        .level     = &leds_levels[0],
        .set_level = set_leds,
        .tid       = 0,
    },
    {
        .pin       = PWM_CHANNEL1,
        .level     = &leds_levels[1],
        .set_level = set_leds,
        .tid       = 0,
    },
};

static struct level_cli button_elems[] = {
    {
        .level = &buttons_states[0],
        .tid   = 0,
    },
    {
        .level = &buttons_states[1],
        .tid   = 0,
    },
    {
        .level = &buttons_states[2],
        .tid   = 0,
    },
    {
        .level = &buttons_states[3],
        .tid   = 0,
    },
};


ic_buttons_device_t buttons_dev = {0};

// Initializing BLE
static const uint8_t dev_uuid[16] = {0xdd, 0xdd};


// Defining the msg publishers
// 2 Bytes for opcode
// 2 Bytes for msg content
// 1 Byte for delay and transient
BT_MESH_MODEL_PUB_DEFINE(gen_onoff_pub_srv_l1, NULL, 2 + 2 + 1);
BT_MESH_MODEL_PUB_DEFINE(gen_onoff_pub_srv_l2, NULL, 2 + 2 + 1);
BT_MESH_MODEL_PUB_DEFINE(gen_onoff_pub_cli_b1, NULL, 2 + 2 + 1);
BT_MESH_MODEL_PUB_DEFINE(gen_onoff_pub_cli_b2, NULL, 2 + 2 + 1);
BT_MESH_MODEL_PUB_DEFINE(gen_onoff_pub_cli_b3, NULL, 2 + 2 + 1);
BT_MESH_MODEL_PUB_DEFINE(gen_onoff_pub_cli_b4, NULL, 2 + 2 + 1);


// Defining config server
static struct bt_mesh_cfg_srv cfg_srv = {
    .relay            = BT_MESH_RELAY_DISABLED,
    .beacon           = BT_MESH_BEACON_ENABLED,
    .frnd             = BT_MESH_FRIEND_NOT_SUPPORTED,
    .gatt_proxy       = BT_MESH_GATT_PROXY_ENABLED,
    .default_ttl      = 7,
    .net_transmit     = BT_MESH_TRANSMIT(2, 20),
    .relay_retransmit = BT_MESH_TRANSMIT(2, 20),
};

// Health server model
BT_MESH_HEALTH_PUB_DEFINE(health_pub, 0);

static struct bt_mesh_health_srv health_srv = {};
static struct bt_mesh_cfg_cli cfg_cli       = {};

static struct bt_mesh_model led1_total[] = {
    BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_SRV, generic_level_srv_op, &gen_onoff_pub_srv_l1,
                  &led_elems[0]),
    BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_CLI, generic_level_cli_op, &gen_onoff_pub_cli_b1,
                  &button_elems[0]),
};

static struct bt_mesh_model but2_tey[] = {
    BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_CLI, generic_level_cli_op, &gen_onoff_pub_cli_b2,
                  &button_elems[1]),
};

static struct bt_mesh_model led2_total[] = {
    BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_SRV, generic_level_srv_op, &gen_onoff_pub_srv_l2,
                  &led_elems[1]),
    BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_CLI, generic_level_cli_op, &gen_onoff_pub_cli_b3,
                  &button_elems[2]),
};
static struct bt_mesh_model but4_tey[] = {
    BT_MESH_MODEL(BT_MESH_MODEL_ID_GEN_LEVEL_CLI, generic_level_cli_op, &gen_onoff_pub_cli_b4,
                  &button_elems[3]),
};


static struct bt_mesh_model root_models[] = {
    BT_MESH_MODEL_CFG_SRV(&cfg_srv),
    BT_MESH_MODEL_CFG_CLI(&cfg_cli),
    BT_MESH_MODEL_HEALTH_SRV(&health_srv, &health_pub),
};

struct bt_mesh_elem elements[] = {
    BT_MESH_ELEM(0, root_models, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, led1_total, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, led2_total, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, but2_tey, BT_MESH_MODEL_NONE),
    BT_MESH_ELEM(0, but4_tey, BT_MESH_MODEL_NONE),
};

static const struct bt_mesh_comp comp = {
    .cid        = BT_COMP_ID_LF,
    .elem       = elements,
    .elem_count = ARRAY_SIZE(elements),
};

static int output_number(bt_mesh_output_action_t action, u32_t number);
static void prov_complete(u16_t net_idx, u16_t addr);
static void prov_reset(void);

static const struct bt_mesh_prov prov = {
    .uuid           = dev_uuid,
    .output_size    = 1,
    .output_actions = BT_MESH_DISPLAY_NUMBER,
    .output_number  = output_number,
    .complete       = prov_complete,
    .reset          = prov_reset,
};

int pin_to_pos(u8_t pin)
{
    if (pin == PWM_CHANNEL0)
        return 0;
    if (pin == PWM_CHANNEL1)
        return 1;
}

void set_leds(u8_t pin, s16_t pulse_width)
{
    if (pulse_width > S16_MAX) {
        pulse_width = S16_MAX;
    } else if (pulse_width < S16_MIN) {
        pulse_width = S16_MIN;
    }
    leds_levels[pin_to_pos(pin)] = pulse_width;
}

static void bt_ready(int err)
{
    if (err) {
        printk("bt_enable init failed with err %d\n", err);
        return;
    }

    printk("Bluetooth initialized.\n");
    err = bt_mesh_init(&prov, &comp);
    if (err) {
        printk("bt_mesh init failed with err %d\n", err);
        return;
    }

    if (IS_ENABLED(CONFIG_SETTINGS)) {
        settings_load();
    }

    /* This will be a no-op if settings_load() loaded provisioning info */
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
    printk("Mesh initialized.\n");
}


// Bluetooth mesh initialization

static int output_number(bt_mesh_output_action_t action, u32_t number)
{
    printk("OOB Number: %u\n", number);
    return 0;
}


static void prov_complete(u16_t net_idx, u16_t addr)
{
    printk("Provisioning was completed.\n");
}

static void prov_reset(void)
{
    bt_mesh_prov_enable(BT_MESH_PROV_ADV | BT_MESH_PROV_GATT);
}

int configure_board()
{
    for (int i = 0; i < 4; i++) {
        button_elems[i].callback_work = pressed_work[i];
    }
    k_work_init(&button_elems[0].callback_work, button_1_pressed_work_handler);
    k_work_init(&button_elems[1].callback_work, button_2_pressed_work_handler);
    k_work_init(&button_elems[2].callback_work, button_3_pressed_work_handler);
    k_work_init(&button_elems[3].callback_work, button_4_pressed_work_handler);
    button_elems[0].model_cli = &led1_total[1];
    button_elems[1].model_cli = &led1_total[2];
    button_elems[2].model_cli = &led2_total[1];
    button_elems[3].model_cli = &led2_total[2];

    pwm_dev = device_get_binding(PWM_DRIVER);
    if (!pwm_dev) {
        printk("Cannot find %s!\n", PWM_DRIVER);
        return -EINVAL;
    }

    ic_buttons_init_device(&buttons_dev);
    ic_buttons_configure(&buttons_dev);
    ic_buttons_configure_callback(&buttons_dev, ic_buttons_callback);


    return 0;
}

void button_1_pressed_work_handler(struct k_work *work)
{
    printk("Button 1 pressed.\n");
    struct level_cli *bt_cli = CONTAINER_OF(work, struct level_cli, callback_work);
    bt_cli->act =
        leds_levels[0] - FADESTEP_LEVEL < S16_MIN ? S16_MIN : leds_levels[0] - FADESTEP_LEVEL;
    send_generic_level_set(bt_cli, BT_MESH_MODEL_OP_GENERIC_LEVEL_SET);
}

void button_2_pressed_work_handler(struct k_work *work)
{
    printk("Button 2 pressed.\n");
    struct level_cli *bt_cli = CONTAINER_OF(work, struct level_cli, callback_work);
    bt_cli->act =
        leds_levels[0] + FADESTEP_LEVEL >= S16_MAX ? S16_MAX : leds_levels[0] + FADESTEP_LEVEL;
    send_generic_level_set(bt_cli, BT_MESH_MODEL_OP_GENERIC_LEVEL_SET);
}


void button_3_pressed_work_handler(struct k_work *work)
{
    printk("Button 3 pressed.\n");
    struct level_cli *bt_cli = CONTAINER_OF(work, struct level_cli, callback_work);
    bt_cli->act =
        leds_levels[1] - FADESTEP_LEVEL < S16_MIN ? S16_MIN : leds_levels[1] - FADESTEP_LEVEL;
    send_generic_level_set(bt_cli, BT_MESH_MODEL_OP_GENERIC_LEVEL_SET);
}

void button_4_pressed_work_handler(struct k_work *work)
{
    printk("Button 4 pressed.\n");
    struct level_cli *bt_cli = CONTAINER_OF(work, struct level_cli, callback_work);
    bt_cli->act =
        leds_levels[1] + FADESTEP_LEVEL >= S16_MAX ? S16_MAX : leds_levels[1] + FADESTEP_LEVEL;
    send_generic_level_set(bt_cli, BT_MESH_MODEL_OP_GENERIC_LEVEL_SET);
}


void ic_buttons_callback(struct device *buttons_device, struct gpio_callback *callback,
                         u32_t button_pin_mask)
{
    time = k_uptime_get_32();

    if (time < last_time + BUTTON_DEBOUNCE_DELAY_MS) {
        last_time = time;
        return;
    }
    k_work_submit(&button_elems[ic_buttons_pin_to_i(button_pin_mask)].callback_work);

    last_time = time;
}


int ic_pwm_run_led(struct device *pwm_dev, u8_t pin, u16_t value)
{
    if (value < FADESTEP)
        value = 0;
    if (value >= PERIOD)
        value = PERIOD;

    int err = pwm_pin_set_usec(pwm_dev, pin, PERIOD, value);
    if (err) {
        printk("pwm pin set fails\n");
        return err;
    }
    return 0;
}

void main(void)
{
    printk("Firmware version: %d.%d.%d\n", ic_version_get_major(), ic_version_get_minor(),
           ic_version_get_build());

    int err;
    err = configure_board();
    if (err) {
        return;
    }

    err = bt_enable(bt_ready);
    if (err) {
        printk("bt_enable failed with err %d\n", err);
        return;
    }
    while (1) {
        ic_pwm_run_led(pwm_dev, led_elems[0].pin,
                       map_level(*led_elems[0].level, S16_MIN, S16_MAX, 0, 20000));
        ic_pwm_run_led(pwm_dev, led_elems[1].pin,
                       map_level(*led_elems[1].level, S16_MIN, S16_MAX, 0, 20000));
        k_sleep(SLEEP_TIME);
    }
}