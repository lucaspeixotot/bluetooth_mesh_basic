/**
 * @file ic_bt.c
 * @Lucas Peixoto (lpdac@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2018-12-25
 *
 * @copyright Copyright (c) 2018
 *
 */

#include "ic_bt.h"

const struct bt_mesh_model_op generic_onoff_cli_op[] = {
    {BT_MESH_MODEL_OP_GENERIC_ONOFF_STATUS, 1, generic_onoff_status},
    BT_MESH_MODEL_OP_END,
};

const struct bt_mesh_model_op generic_onoff_srv_op[] = {
    {BT_MESH_MODEL_OP_GENERIC_ONOFF_GET, 0, generic_onoff_get},
    {BT_MESH_MODEL_OP_GENERIC_ONOFF_SET, 2, generic_onoff_set},
    {BT_MESH_MODEL_OP_GENERIC_ONOFF_SET_UNACK, 2, generic_onoff_set_unack},
    BT_MESH_MODEL_OP_END,
};
const struct bt_mesh_model_op generic_level_srv_op[] = {
    {BT_MESH_MODEL_OP_GENERIC_LEVEL_GET, 0, generic_level_get},
    {BT_MESH_MODEL_OP_GENERIC_LEVEL_SET, 2, generic_level_set},
    {BT_MESH_MODEL_OP_GENERIC_LEVEL_SET_UNACK, 2, generic_level_set_unack},
    BT_MESH_MODEL_OP_END,
};

const struct bt_mesh_model_op generic_level_cli_op[] = {
    {BT_MESH_MODEL_OP_GENERIC_ONOFF_STATUS, 1, generic_level_status},
    BT_MESH_MODEL_OP_END,
};

void generic_onoff_get(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *buf)
{
    printk("[0x%04x]: Received a get msg from group address 0x%04x, sended by 0x%04x.\n",
           bt_mesh_model_elem(model)->addr, ctx->recv_dst, ctx->addr);
    struct onoff_srv *cur_elem = model->user_data;


    // 2 bytes for the opcode
    // 1 bytes parameters: present onoff value
    // 2 optional bytes for target onoff and remaining time
    // 4 additional bytes for the TransMIC

    u8_t buflen = 7;

    NET_BUF_SIMPLE_DEFINE(msg, buflen);

    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GENERIC_ONOFF_STATUS);
    net_buf_simple_add_u8(&msg, !(*cur_elem->state));

    if (bt_mesh_model_send(model, ctx, &msg, NULL, NULL)) {
        printk("Unable to send generic onoff status message\n");
    }

    printk("[0x%04x]: Get message value -> %d.\n", bt_mesh_model_elem(model)->addr,
           !(*cur_elem->state));
}


void generic_onoff_set(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *buf)
{
    generic_onoff_set_unack(model, ctx, buf);
    generic_onoff_get(model, ctx, buf);
}


void generic_onoff_set_unack(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                             struct net_buf_simple *buf)
{
    printk("[0x%04x]: Received a set msg from group address 0x%04x, sended by 0x%04x.\n",
           bt_mesh_model_elem(model)->addr, ctx->recv_dst, ctx->addr);
    struct onoff_srv *elem = model->user_data;
    u8_t buflen            = buf->len;
    u8_t new_onoff_state   = net_buf_simple_pull_u8(buf);
    u8_t tid               = net_buf_simple_pull_u8(buf);

    if (buflen > 2) {
        printk("[wrn]: message contains transition_time field - processing not implemented\n");
    }

    if (buflen > 3) {
        printk("[wrn]: message contains delay field - processing not implemented\n");
    }

    elem->set_state(elem->pos, new_onoff_state);
    printk("[0x%04x]: Value msg -> %d/ TID -> %d\n", bt_mesh_model_elem(model)->addr,
           new_onoff_state, tid);
}


void generic_onoff_status(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                          struct net_buf_simple *buf)
{
    u8_t state               = net_buf_simple_pull_u8(buf);
    struct onoff_cli *bt_cli = model->user_data;
    *bt_cli->state           = state;

    printk("Node 0x%04x OnOff status from 0x%04x with state 0x%02x\n",
           bt_mesh_model_elem(model)->addr, ctx->addr, state);
}

u16_t map_level(s16_t x, s16_t in_min, s16_t in_max, u16_t out_min, u16_t out_max)
{
    return (u16_t)((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}

void generic_level_get(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *buf)
{
    printk("[0x%04x]: Received a get level msg from group address 0x%04x, sended by 0x%04x.\n",
           bt_mesh_model_elem(model)->addr, ctx->recv_dst, ctx->addr);
    struct level_srv *elem = model->user_data;

    // 2 bytes for the opcode
    // 1 bytes parameters: present onoff value
    // 2 optional bytes for target onoff and remaining time
    // 4 additional bytes for the TransMIC

    u8_t buflen = 2 + 2 + 4;

    NET_BUF_SIMPLE_DEFINE(msg, buflen);

    bt_mesh_model_msg_init(&msg, BT_MESH_MODEL_OP_GENERIC_LEVEL_STATUS);
    net_buf_simple_add_le16(&msg, *elem->level);

    printk("[0x%04x]: Get message value -> %d e %d.\n", bt_mesh_model_elem(model)->addr,
           map_level(*elem->level, S16_MIN, S16_MAX, 0, 100), *elem->level);

    if (bt_mesh_model_send(model, ctx, &msg, NULL, NULL)) {
        printk("Unable to send generic onoff status message\n");
    }
}


void generic_level_set(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                       struct net_buf_simple *buf)
{
    generic_level_set_unack(model, ctx, buf);
    generic_level_get(model, ctx, buf);
}


void generic_level_set_unack(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                             struct net_buf_simple *buf)
{
    printk("[0x%04x]: Received a set level msg from group address 0x%04x, sended by 0x%04x.\n",
           bt_mesh_model_elem(model)->addr, ctx->recv_dst, ctx->addr);
    struct level_srv *elem = model->user_data;
    u8_t buflen            = buf->len;
    s16_t new_level_state  = net_buf_simple_pull_le16(buf);
    u8_t tid               = net_buf_simple_pull_u8(buf);

    if (buflen > 3) {
        printk("[wrn]: message contains transition_time field - processing not implemented\n");
    }

    if (buflen > 4) {
        printk("[wrn]: message contains delay field - processing not implemented\n");
    }

    elem->set_level(elem->pin, new_level_state);
    printk("[0x%04x]: Value msg -> %d / TID -> %d\n", bt_mesh_model_elem(model)->addr,
           map_level(new_level_state, S16_MIN, S16_MAX, 0, 100), tid);
}


void generic_level_status(struct bt_mesh_model *model, struct bt_mesh_msg_ctx *ctx,
                          struct net_buf_simple *buf)
{
    s16_t level              = net_buf_simple_pull_le16(buf);
    struct level_cli *bt_cli = model->user_data;
    *bt_cli->level           = level;
    printk("Node 0x%04x level status from 0x%04x with level %d / %d\n",
           bt_mesh_model_elem(model)->addr, ctx->addr, level,
           map_level(level, S16_MIN, S16_MAX, 0, 100));
}

void send_generic_onoff_get(struct onoff_cli *bt_cli, u16_t message_type)
{
    struct bt_mesh_model_pub *pub_cli;
    pub_cli = bt_cli->model_cli->pub;
    printk("Sending onoff get msg to 0x%04x\n", pub_cli->addr);
    bt_mesh_model_msg_init(pub_cli->msg, message_type);
    int err = bt_mesh_model_publish(bt_cli->model_cli);
    if (err) {
        printk("bt_mesh_model_publish err %d, sending msg to 0x%04x\n", err, pub_cli->addr);
    }
}

void send_generic_onoff_set(struct onoff_cli *bt_cli, u16_t message_type)
{
    struct bt_mesh_model_pub *pub_cli;
    pub_cli = bt_cli->model_cli->pub;
    printk("Sending onoff set msg to 0x%04x, value -> %d\n", pub_cli->addr, *bt_cli->state);
    bt_mesh_model_msg_init(pub_cli->msg, message_type);
    net_buf_simple_add_u8(pub_cli->msg, !(*bt_cli->state));
    net_buf_simple_add_u8(pub_cli->msg, bt_cli->tid++);
    int err = bt_mesh_model_publish(bt_cli->model_cli);
    if (err) {
        printk("bt_mesh_model_publish err %d, sending msg to 0x%04x\n", err, pub_cli->addr);
    }
}

void send_generic_level_set(struct level_cli *bt_cli, u16_t message_type)
{
    struct bt_mesh_model_pub *pub_cli;
    pub_cli = bt_cli->model_cli->pub;
    printk("Sending level set msg to 0x%04x, value -> %d / %d\n", pub_cli->addr, bt_cli->act,
           map_level(bt_cli->act, S16_MIN, S16_MAX, 0, 100));
    bt_mesh_model_msg_init(pub_cli->msg, message_type);
    net_buf_simple_add_le16(pub_cli->msg, bt_cli->act);
    net_buf_simple_add_u8(pub_cli->msg, bt_cli->tid++);
    int err = bt_mesh_model_publish(bt_cli->model_cli);
    if (err) {
        printk("bt_mesh_model_publish err %d, sending msg to 0x%04x\n", err, pub_cli->addr);
    }
}
void send_generic_level_get(struct level_cli *bt_cli, u16_t message_type)
{
    struct bt_mesh_model_pub *pub_cli;
    pub_cli = bt_cli->model_cli->pub;
    printk("Sending level get msg to 0x%04x\n", pub_cli->addr);
    bt_mesh_model_msg_init(pub_cli->msg, message_type);
    int err = bt_mesh_model_publish(bt_cli->model_cli);
    if (err) {
        printk("bt_mesh_model_publish err %d, sending msg to 0x%04x\n", err, pub_cli->addr);
    }
}